# operation-tracker
A **Java** library that must be used with **Spring Boot** and APM agent. It will send tracker operation to an Manager server to log listener kafka topics async.
# Usage

In your pom.xml use:

``` xml
 <dependency>
       <groupId>com.gitlab.klabs</groupId>
       <artifactId>operation-tracker</artifactId>
       <version>1.1.1</version>
 </dependency>
```

and

``` xml
<repositories>
   <repository>
     <id>jitpack.io</id>
     <url>https://jitpack.io</url>
   </repository>
</repositories>
```

and in your main class:

```java
@SpringBootApplication
@EnableOperationTracker
public class Application{
  //code here
}
```

In your Spring Boot Rest Controller you can use:

```java
@OperationTracker
@KafkaListener(topics = "${spring.kafka.topic.....}")
@CaptureTransaction
public void listen(String message) {
   //your code here
}
```

# Issues

In case you face exceptions related to JoinPoint/CodeSignature class in your application initialization try to put the following dependency in your **pom.xml**:

``` xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-aop</artifactId>
</dependency>
```

# Generating another version

1 - Create a new branch from the master branch.

2 - Make your modifications.

3 - Update the **pom.xml** version section, the **README.md** file, tag your last commit with the new version and push everything.

4 - Merge the code into the master branch. Check the permissions to do it.