package com.klabs.operationtracker.dto;

import com.mongodb.BasicDBObject;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.Map;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class OperationsDetails<T extends Serializable> implements Serializable {

    private static final long serialVersionUID = -479047694458682556L;

    @NotBlank(message = "Target não pode ser nulo ou vazio")
    private String target;
    private String verb;
    private BasicDBObject payload;
    private Map<String, T> headers;

}
