package com.klabs.operationtracker.request;

import com.klabs.operationtracker.dto.OperationsDetails;
import com.klabs.operationtracker.enums.OperationsStatusEnum;
import com.klabs.operationtracker.enums.OperationsTypeEnum;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Map;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class RegisterOperationsRequest<T extends Serializable> implements Serializable {

    private static final long serialVersionUID = -4816065707578100637L;

    private String key;
    @NotBlank(message = "O campo 'clientId' é obrigatório e não pode ser vazio")
    private String clientId;
    @NotNull(message = "O campo 'type' é obrigatório e não pode ser vazio")
    private OperationsTypeEnum type;
    @Builder.Default
    private OperationsStatusEnum status = OperationsStatusEnum.PENDING;
    @NotNull(message = "O campo 'details' é obrigatório e não pode ser vazio")
    private OperationsDetails<?> details;
    private Map<String, T> data;
}