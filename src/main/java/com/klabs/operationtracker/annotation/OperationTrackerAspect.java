package com.klabs.operationtracker.annotation;

import co.elastic.apm.api.CaptureSpan;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.klabs.operationtracker.dto.OperationsDetails;
import com.klabs.operationtracker.enums.OperationsStatusEnum;
import com.klabs.operationtracker.enums.OperationsTypeEnum;
import com.klabs.operationtracker.exception.UnableToRegisterOperationException;
import com.klabs.operationtracker.request.RegisterOperationsRequest;
import com.mongodb.BasicDBObject;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Slf4j
@Aspect
@Component
public class OperationTrackerAspect {

    private static final String ERROR_CODE = "annotation TrackerOperationAspect";
    private static final String TOPIC_REGISTER_OPERATION = "register-operation";
    private static final String PARAMETER_KAFKA_NAME = "message";

    @Autowired
    private OperationTrackerKafkaSender operationTrackerKafkaSender;

    @Autowired
    ConfigurableBeanFactory beanFactory;

    @Value("${spring.application.name}")
    private String appName;


    @CaptureSpan
    @Before("@annotation(operationTracker)")
    public void pendingOperations(JoinPoint joinPoint, OperationTracker operationTracker){
        log.info("@TrackerOperation: pendingOperations");
        registerOperation(joinPoint, OperationsStatusEnum.PENDING, null);
    }

    @CaptureSpan
    @AfterReturning(pointcut = "@annotation(operationTracker)")
    public void successOperations(JoinPoint joinPoint, OperationTracker operationTracker) {
        log.info("@TrackerOperation: successOperations");
        registerOperation(joinPoint,OperationsStatusEnum.SUCCESS, null);
    }


    @CaptureSpan
    @AfterThrowing(pointcut = "@annotation(operationTracker)", throwing="exception")
    public void errorOperations(JoinPoint joinPoint, OperationTracker operationTracker, Throwable exception) {
        log.info("@TrackerOperation: errorOperations");
        registerOperation(joinPoint,OperationsStatusEnum.ERROR, exception);
    }

    private void registerOperation(JoinPoint joinPoint, OperationsStatusEnum status, Throwable exception){

        List<String> topicTargets = getKafkaTopic(joinPoint);

        topicTargets.forEach(
                target -> {
                    String topic  = getValueEnvironment(target);
                    Map params = getMapParameters(joinPoint);

                    Map<String, Object> dataException = new HashMap<>();
                    if (status.equals(OperationsStatusEnum.ERROR)) {
                        dataException.put(exception.getClass().getSimpleName(), exception.getMessage());
                    }

                    RegisterOperationsRequest request = getOperationsRequest(
                            status,
                            OperationsTypeEnum.KAFKA,
                            topic,
                            params,
                            dataException
                    );

                    sendKafkaOperation(request);
                }
        );

    }

    private List<String> getKafkaTopic(JoinPoint joinPoint){

        Optional<List<String>> topics =  Optional.ofNullable(((MethodSignature) joinPoint.getSignature())
                .getMethod()
                .getAnnotation(KafkaListener.class)
                .topics()
        ).map(Arrays::asList);
        return topics.orElseGet(ArrayList::new);
    }

    private Map getMapParameters(JoinPoint joinPoint) {

        String[] argNames = ((MethodSignature) joinPoint.getSignature()).getParameterNames();
        Object[] values = joinPoint.getArgs();
        Map<String, Object> params = new HashMap<>();

        if (argNames.length != 0) {
            for (int i = 0; i < argNames.length; i++) {
                params.put(argNames[i], new String(String.valueOf(values[i]).getBytes(), StandardCharsets.UTF_8));
            }

        }
        return params;
    }

    private RegisterOperationsRequest getOperationsRequest(OperationsStatusEnum status,
                                                           OperationsTypeEnum type,
                                                           String target,
                                                           Map params,
                                                           Map data) {
        RegisterOperationsRequest registerOperationsRequest = RegisterOperationsRequest.builder()
                .clientId(appName)
                .status(status)
                .type(type)
                .details(
                        OperationsDetails.builder()
                                .target(target)
                                .build()
                )
                .data(data)
                .build();

        String json = String.valueOf(params.get(PARAMETER_KAFKA_NAME));
        JsonObject convertedObject = new Gson().fromJson(json, JsonObject.class);
        registerOperationsRequest.getDetails().setPayload(BasicDBObject.parse(new Gson().toJson(convertedObject)));
        return registerOperationsRequest;
    }

    private String getValueEnvironment(String chave){
        return beanFactory.resolveEmbeddedValue(chave);
    }

    private void sendKafkaOperation(RegisterOperationsRequest request) {
        try {
            operationTrackerKafkaSender.send(TOPIC_REGISTER_OPERATION , request );
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new UnableToRegisterOperationException(e.getMessage(),ERROR_CODE);
        }
    }
}
