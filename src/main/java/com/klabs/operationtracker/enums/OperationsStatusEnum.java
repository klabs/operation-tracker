package com.klabs.operationtracker.enums;

public enum OperationsStatusEnum {

    PENDING,
    SUCCESS,
    ERROR;

}
