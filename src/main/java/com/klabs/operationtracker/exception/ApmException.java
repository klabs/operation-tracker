package com.klabs.operationtracker.exception;

import co.elastic.apm.api.ElasticApm;

public class ApmException extends RuntimeException {
	protected final String errorCode;

	public ApmException(String msg, String errorCode) {
		super(msg);
		this.errorCode = errorCode;
		ElasticApm.currentSpan().captureException(this);
	}
}
