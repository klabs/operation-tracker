package com.klabs.operationtracker.exception;



public class UnableToRegisterOperationException extends ApmException {
    public UnableToRegisterOperationException(String message, String code) {
        super(message, code);
    }
}
